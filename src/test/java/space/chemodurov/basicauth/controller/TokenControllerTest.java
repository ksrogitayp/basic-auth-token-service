package space.chemodurov.basicauth.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import space.chemodurov.basicauth.service.impl.TokenServiceImpl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ContextConfiguration(classes = {TokenController.class, TokenServiceImpl.class})
@WebMvcTest
class TokenControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("Getting token. Everything OK")
    void getToken_ok() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/v1/base-64-token")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"username\": \"user\", \"password\":\"pass\" }")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo("Basic dXNlcjpwYXNz");
    }

    @Test
    @DisplayName("Validation error. Field username is blank.")
    void getToken_emptyUsername() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/v1/base-64-token")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"username\": \"\", \"password\":\"pass\" }")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo("{\"username\":\"field 'username' must be not empty\"}");
    }

    @Test
    @DisplayName("Validation error. Field username is null.")
    void getToken_username_is_null() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/v1/base-64-token")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"password\":\"pass\" }")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo("{\"username\":\"field 'username' must be not empty\"}");
    }

    @Test
    @DisplayName("Validation error. Field password is blank.")
    void getToken_emptyPassword() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/v1/base-64-token")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"username\": \"user\", \"password\":\"\" }")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo("{\"password\":\"field 'password' must be not empty\"}");
    }

    @Test
    @DisplayName("Validation error. Field password is too long.")
    void getToken_hugePassword() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/v1/base-64-token")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"username\": \"user\", \"password\":\"qwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnm\" }")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo("{\"password\":\"password is too long\"}");
    }
}