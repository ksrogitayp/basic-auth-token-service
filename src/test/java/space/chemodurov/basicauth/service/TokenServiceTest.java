package space.chemodurov.basicauth.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import space.chemodurov.basicauth.entity.Credentials;
import space.chemodurov.basicauth.service.impl.TokenServiceImpl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ContextConfiguration(classes = TokenServiceImpl.class)
@ExtendWith({SpringExtension.class})
class TokenServiceTest {

    @Autowired
    private TokenService tokenService;

    @Test
    @DisplayName("Work fine")
    void getToken() {
        String result = tokenService.getToken(new Credentials().setUsername("user").setPassword("pass"));
        assertThat("Basic dXNlcjpwYXNz").isEqualTo(result);
    }

    @Test
    @DisplayName("Check there are no npe")
    void getToken_npe() {
        String result = tokenService.getToken(null);
        assertThat(result).isBlank();
    }
}