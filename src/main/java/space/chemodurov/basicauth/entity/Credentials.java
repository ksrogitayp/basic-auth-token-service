package space.chemodurov.basicauth.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Accessors(chain = true)
public class Credentials {

    @NotBlank(message = "field 'username' must be not empty")
    @Size(max = 255, message = "username is too long")
    private String username;

    @NotBlank(message = "field 'password' must be not empty")
    @Size(max = 255, message = "password is too long")
    private String password;

}
