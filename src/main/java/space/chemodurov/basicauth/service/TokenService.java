package space.chemodurov.basicauth.service;

import space.chemodurov.basicauth.entity.Credentials;

public interface TokenService {

    String getToken(Credentials credentials);

}
