package space.chemodurov.basicauth.service.impl;

import org.springframework.stereotype.Service;
import space.chemodurov.basicauth.entity.Credentials;
import space.chemodurov.basicauth.service.TokenService;

import java.util.Base64;

@Service
public class TokenServiceImpl implements TokenService {

    @Override
    public String getToken(Credentials creds) {

        if (creds == null) return "";

        return "Basic " + Base64.getEncoder()
                .encodeToString(String.format("%s:%s", creds.getUsername(), creds.getPassword()).getBytes());
    }
}
